// Switcher for swiching switches

// Make sure that these are exactly 16 as the aussumption will be made later
int pins[16] = {2,3,4,5, 6,7,8,9, A0,A1,A2,A3, 10,11,12,13};

void setup() {
	Serial.begin(9600);
	for (int i = 0; i < 16;i++) {
		pinMode(pins[i], OUTPUT);
	}
}

void loop() {
	if (Serial.available()) {
		byte raw = (byte) Serial.read();
		byte pin = raw & 0x0F;
		byte command = raw & 0xF0;
		Serial.println("Received: RAW:"+String(raw, HEX)+" PIN:"+String(pin,HEX)+" CMD:"+String(command,HEX));
		//off command, `,a to o
		if (command == 0x60) {
			digitalWrite(pins[pin], false);
		//on command @,A to O
		} else if (command == 0x40) {
			digitalWrite(pins[pin], true);
		}
	}
	delay(10);
}
