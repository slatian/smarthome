#!/bin/python3

import serial
import sys
import struct

def usage():
	print("send_code.py <pin> <on|off>")
	sys.exit(2)

if len(sys.argv) != 3:
	usage()
	
pin = int(sys.argv[1])
state = sys.argv[2]
print(pin, state)

if (pin < 0 or pin > 15 or not (state=="on" or state=="off")):
	usage()

command = 0x60
if state == "on":
	command = 0x40

line = serial.Serial("/dev/ttyACM0", 9600)
line.write(struct.pack("B", pin | command))
