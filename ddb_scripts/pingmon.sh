host="$1"
object_name="$2"
idle_time="$3"

help() {
cat <<EOF
Usage:
	ddb_pingmon <host> <object_name> [<idle_time>]

Ping <host> and exports an online/offline status as a ddb object.
Also emits a ping signal on the object just before sending the ping.
Waits <idle_time> seconds after reveiving a ping or timing out
before pinging again, defaults to 30 seconds.
EOF
exit
}

[ -z "$host" ] && help
[ -z "$object_name" ] && help
[ -z "$idle_time" ] && idle_time=30

echo "> $object_name type pingmon"
echo "> $object_name hostname $host"

# NOTE: depending on your version of awk it'll need the `-W interactive` option or complain about and ignore it (and just work or not)
# ping -W "$idle_time" -i "$idle_time" "$host" 2>/dev/null | awk -v "object_name=$object_name" -W interactive '!/^PING /{ print "s " object_name " ping" ; if ( $2 == "bytes" && $3 == "from" ) { print "> " object_name " status online" } else { print "> " object_name " status offline" } }' 2> /dev/null

# deboune by allowing multiple pings to fail before considering the machine offline
online_score=0

# This may have to be restarted with a cronjob after a few hundred pings for some reason
while true; do
	echo "s $object_name ping"
	ping_start_time="$(date +"%s")"
	if ping -W "$idle_time" -c 1 "$host" >/dev/null 2>/dev/null
	then
		[[ "$online_score" -lt 5 ]] &&
		online_score="$((online_score+1))" || true
	else
		[[ "$online_score" -gt 0 ]] &&
		online_score="$((online_score-1))" || true
	fi
	ddb write_value "$object_name" online_score "$online_score"

	if [[ "$online_score" -gt 0 ]]
	then
 		ddb write_value "$object_name" status online
	else
  		ddb write_value "$object_name" status offline
    fi

	if ping -W "$idle_time" -c 1 "$host" >/dev/null 2>/dev/null
	then
		echo "> $object_name status online"
	else
		echo "> $object_name status offline"
	fi
	ping_end_time="$(date +"%s")"
	sleep_time="$((idle_time-(ping_end_time-ping_start_time)))"
	[ "$sleep_time" -gt 0 ] && sleep "$sleep_time"
done
