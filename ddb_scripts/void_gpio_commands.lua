io.read()
print("+ asw")
io.read()
print("> asw type register_file")
io.read()
print("+ outlets")
io.read()
print("> outlets type register_file")
io.read()

local commands = {}

local function add_gpio_pin(name, pin)
	commands["> "..name.." on"] = "set_yc_switch.py  "..pin.." on > /dev/null"
	commands["> "..name.." off"] = "set_yc_switch.py "..pin.." off > /dev/null"
	print("r "..name)
end

add_gpio_pin("asw x", 11)
add_gpio_pin("asw y", 10)
add_gpio_pin("asw z", 9)

add_gpio_pin("asw l", 8)

add_gpio_pin("outlets 1", 0)
add_gpio_pin("outlets 2", 1)
add_gpio_pin("outlets 3", 2)
add_gpio_pin("outlets 4", 3)
add_gpio_pin("outlets 5", 4)
add_gpio_pin("outlets 6", 5)
add_gpio_pin("outlets 7", 6)
add_gpio_pin("outlets 8", 7)

while true do
	local i = io.read()
	if not i then break end
	local command = commands[i]
	if command then
		os.execute(command)
	end
end
