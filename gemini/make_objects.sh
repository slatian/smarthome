mkdir -p ./srv/obj

on_off () {
	bash ./make_on_off_device.sh ./srv/obj/"$1" "$2" "$3"
}

ddb_on_off () {
	bash ./make_ddb_on_off_device.sh ./srv/obj/"$1" "$2" "$3"
}

ddb_switch () {
	bash ./make_ddb_switch_device.sh ./srv/obj/"$1" "$2" "$3" "$4"
}

# type endpoint_name arg class arg

ddb_on_off audio_amber "requests audio-amber" audio_switch
ddb_on_off audio_tv    "requests audio-tv"    audio_switch
ddb_on_off audio_aux   "requests audio-aux"   audio_switch

ddb_switch amp_mode "requests amp_mode" mode_switch "J T JS JT"

on_off asw_x 21 audio_switch
on_off asw_y 20 audio_switch
on_off asw_z 16 audio_switch
on_off asw_l 12 light

on_off outlet_1  7 power_switch
on_off outlet_2  8 power_switch
on_off outlet_3 25 power_switch
on_off outlet_4 24 power_switch
on_off outlet_5 23 power_switch
on_off outlet_6 18 power_switch
on_off outlet_7 15 power_switch
on_off outlet_8 14 power_switch
