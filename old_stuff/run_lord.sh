#!/bin/sh
lord start ddb

ddb_cat << EOF
> requests type register file
> requests amp_mode J
> requests audio-amber off
> requests audio-tv off
> requests audio-aux off
EOF

lord start ddb-gpio-commands
lord start ddb-pingmon-amber
lord start ddb-logic
lord start molly
lord_daemon
